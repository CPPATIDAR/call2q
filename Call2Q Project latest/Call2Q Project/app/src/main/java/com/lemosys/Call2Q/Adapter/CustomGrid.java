package com.lemosys.Call2Q.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lemosys.Call2Q.AddGroupActivity;
import com.lemosys.Call2Q.Database.DatabaseHelper;
import com.lemosys.Call2Q.GroupActivity;
import com.lemosys.Call2Q.Pojos.GroupModel;
import com.lemosys.Call2Q.R;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CustomGrid extends BaseAdapter {

    Context context;
    List<GroupModel> groupModels = new ArrayList<>();
    private int[] mMaterialColors;
    DatabaseHelper databaseHelper;

    public CustomGrid(Context context,List<GroupModel> groupModels) {
        this.context = context;
        this.groupModels = groupModels;
        mMaterialColors = context.getResources().getIntArray(R.array.colors);
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        return groupModels.size();
    }

    @Override
    public Object getItem(int i) {
        return groupModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.grid_layout, null);
        ImageView group_image;
        MaterialLetterIcon materialLetterIcon,count;
        RelativeLayout relativeLayout;
        relativeLayout = view.findViewById(R.id.grid_view_custom);
        materialLetterIcon = view.findViewById(R.id.material_icon);
        TextView name,type;
        count = view.findViewById(R.id.count);
        group_image = view.findViewById(R.id.profile_image);
        name = view.findViewById(R.id.group_name);
        type = view.findViewById(R.id.group_type);
        name.setText(groupModels.get(i).getName());
        count.setLetter(String.valueOf(groupModels.get(i).getCount()));
        type.setText(groupModels.get(i).getType());
        materialLetterIcon.setLettersNumber(2);
        materialLetterIcon.setShapeColor(mMaterialColors[new Random().nextInt(mMaterialColors.length)]);
       relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View view) {

               return false;
           }
       });
        materialLetterIcon.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(view.getRootView().getContext());
                alertbox.setMessage("Select an Option");
                alertbox.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int l) {
                        if (groupModels.size()>0){
                            String id = String.valueOf(groupModels.get(i).getId());
                            databaseHelper.deleteGroup(id);
                            ((AddGroupActivity)context).refreshScreen();
                            groupModels.remove(i);
                        }
                    }
                });
                alertbox.setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int l) {
                        ((AddGroupActivity)context).editGroup(groupModels.get(i).getId(),groupModels.get(i).getName(),groupModels.get(i).getType(),groupModels.get(i).getIcon(),groupModels.get(i).getCount());
                    }
                });
                alertbox.show();
                return false;
            }
        });
        group_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(view.getRootView().getContext());
                alertbox.setMessage("Select an Option");
                alertbox.setTitle("Warning");
                alertbox.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int l) {
                        if (groupModels.size()>0){
                            String id = String.valueOf(groupModels.get(i).getId());
                            databaseHelper.deleteGroup(id);
                            ((AddGroupActivity)context).refreshScreen();
                            groupModels.remove(i);
                        }
                    }
                });
                alertbox.setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int l) {
                        ((AddGroupActivity)context).editGroup(groupModels.get(i).getId(),groupModels.get(i).getName(),groupModels.get(i).getType(),groupModels.get(i).getIcon(),groupModels.get(i).getCount());
                    }
                });
                alertbox.show();
                return false;
            }
        });
        final int color = materialLetterIcon.getShapeColor();
        if (groupModels.get(i).getIcon()==null){
            group_image.setVisibility(View.GONE);
            materialLetterIcon.setLetter(groupModels.get(i).getName());
            materialLetterIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, GroupActivity.class);
                    intent.putExtra("color",color);
                    intent.putExtra("name",groupModels.get(i).getName());
                    int id = groupModels.get(i).getId();
                    intent.putExtra("group_id",id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }else {
            materialLetterIcon.setVisibility(View.INVISIBLE);
               Bitmap bitmap = getImage(groupModels.get(i).getIcon());
               group_image.setImageBitmap(bitmap);
               group_image.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       Intent intent = new Intent(context, GroupActivity.class);
                       intent.putExtra("name",groupModels.get(i).getName());
                       intent.putExtra("color",color);
                       int id = groupModels.get(i).getId();
                       intent.putExtra("group_id",id);
                       intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       context.startActivity(intent);
                   }
               });
        }

        return view;
    }

    public Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

}
