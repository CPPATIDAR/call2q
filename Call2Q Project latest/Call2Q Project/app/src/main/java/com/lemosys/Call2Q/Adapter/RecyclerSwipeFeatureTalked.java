package com.lemosys.Call2Q.Adapter;

import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

public class RecyclerSwipeFeatureTalked extends ItemTouchHelper.SimpleCallback {

    SwipeListenertalked swipeListenertalked_listener;

    public RecyclerSwipeFeatureTalked(int dragDirs, int swipeDirs,SwipeListenertalked swipeListenertalked) {
        super(dragDirs, swipeDirs);
        this.swipeListenertalked_listener = swipeListenertalked;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        swipeListenertalked_listener.onSwipedListener(viewHolder,i,viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        View foreground = ((RecyclerAdapterTalked.MyViewHolder)viewHolder).rl_foreground_talked;
        getDefaultUIUtil().onDraw(c,recyclerView,foreground,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        View foreground = ((RecyclerAdapterTalked.MyViewHolder)viewHolder).rl_foreground_talked;
        getDefaultUIUtil().clearView(foreground);
    }

    @Override
    public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        View foreground = ((RecyclerAdapterTalked.MyViewHolder)viewHolder).rl_foreground_talked;
        getDefaultUIUtil().onDrawOver(c,recyclerView,foreground,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
        super.onSelectedChanged(viewHolder, actionState);
        if (viewHolder!=null){
            View foreground = ((RecyclerAdapterTalked.MyViewHolder)viewHolder).rl_foreground_talked;
            getDefaultUIUtil().onSelected(foreground);
        }
    }

    public interface SwipeListenertalked{
        void onSwipedListener(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
