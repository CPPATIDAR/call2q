package com.lemosys.Call2Q.Pojos;

import java.io.Serializable;

public class ContactModel implements Serializable{

    String name,number;
    String id;
    boolean isCalled = false;
    String group_id,IsActive,IsDisconnected;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public ContactModel() {

    }

    public ContactModel(String name, String number) {

        this.name = name;
        this.number = number;
    }

    public boolean isCalled() {
        return isCalled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public void setCalled(boolean called) {
        isCalled = called;
    }

    @Override
    public int hashCode() {
        System.out.println("In hashcode "+"value is :"+this.name);
        int hash = 3;
        hash = 7 * hash + this.name.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        ContactModel contactModel = (ContactModel) obj;
        if(contactModel.getName().equals(this.name)){
            return true;
        }
        return false;
    }

    public ContactModel(String name, String number, String group_id) {
        this.name = name;
        this.number = number;
        this.group_id = group_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {

        this.group_id = group_id;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getIsDisconnected() {
        return IsDisconnected;
    }

    public void setIsDisconnected(String isDisconnected) {
        IsDisconnected = isDisconnected;
    }
}
