package com.lemosys.Call2Q;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lemosys.Call2Q.Adapter.CustomGrid;
import com.lemosys.Call2Q.Database.DatabaseHelper;
import com.lemosys.Call2Q.Dialog.GroupAddDialog;
import com.lemosys.Call2Q.Pojos.GroupModel;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.List;

public class AddGroupActivity extends AppCompatActivity implements GroupAddDialog.dialogListener {

    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS, Manifest.permission.READ_EXTERNAL_STORAGE , Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_CALL_LOG,Manifest.permission.CAMERA};
    Button button;
    TextView t1,t2,t3,t4,t5,t6,header_tv,done;
    TextView reset;
    View view;
    GroupAddDialog groupAddDialog1;
    GroupAddDialog groupAddDialog;
    DatabaseHelper databaseHelper;
    RelativeLayout relativeLayout;
    GridView gridView;
    CustomGrid customGrid;
    private  android.app.AlertDialog.Builder alertbox;
    AdView adView;
    List<GroupModel> groupModels= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);

        initComponents();
        initListeners();
        permission();

        MobileAds.initialize(this, "ca-app-pub-5615829142679905~9312512301");


    }

    public void permission(){
        if(!checkPermission(PERMISSIONS)){
            ActivityCompat.requestPermissions(AddGroupActivity.this, PERMISSIONS, PERMISSION_ALL);
        }


    }
    private void initListeners() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    groupAddDialog  = new GroupAddDialog();
                    groupAddDialog.show(getSupportFragmentManager(),"Group");
            }
        });

    }

    private void initComponents() {
        alertbox = new android.app.AlertDialog.Builder(AddGroupActivity.this);

        databaseHelper = new DatabaseHelper(getApplicationContext());
        button = findViewById(R.id.btn);
        t1 = findViewById(R.id.tv);
        t2 = findViewById(R.id.tv1);
        t3 = findViewById(R.id.tv2);
        t4 = findViewById(R.id.tv3);
        t5 = findViewById(R.id.tv4);
        t6 = findViewById(R.id.tv5);
        header_tv = findViewById(R.id.header_tv);
        reset = findViewById(R.id.reset);
        done = findViewById(R.id.done);
        adView = findViewById(R.id.adView);
        view = findViewById(R.id.view);

        relativeLayout = findViewById(R.id.relative);
        gridView = findViewById(R.id.grid_view);
        groupModels = databaseHelper.getGroup();



        Drawable drawable = getResources().getDrawable(R.drawable.backg);
        relativeLayout.setBackground(drawable);
        if (groupModels.size()>0)
        {
            customGrid = new CustomGrid(this,groupModels);
            gridView.setAdapter(customGrid);
        }

        header_tv.setText("");
        done.setVisibility(View.GONE);
        reset.setVisibility(View.GONE);

        AdRequest adRequest = new  AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }


    @Override
    public int onSubmitClick(String name, String type,byte[] img) {
        int id = databaseHelper.addGroup(name,type,img,0);
        groupAddDialog.dismiss();
        return id;
    }

    @Override
    public int onEditClick(String name, String type, byte[] img, String id, int count) {
        databaseHelper.editGroup(id,name,type,img,count);
        groupAddDialog1.dismiss();
        groupModels = databaseHelper.getGroup();
        if (groupModels.size()>0)
        {
            customGrid = new CustomGrid(this,groupModels);
            gridView.setAdapter(customGrid);
        }
        return 0;
    }


    public boolean checkPermission(String... permission){

        for (String permissions: permission){
            if (ContextCompat.checkSelfPermission(AddGroupActivity.this,permissions)
                    != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.e("TAG", "onRequestPermissionsResult: "+grantResults.length );
        for (int i=0;i<grantResults.length;i++){
            if (grantResults[i]==PackageManager.PERMISSION_GRANTED){

            }else {
                button.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar
                        .make(relativeLayout,  " Allow all permission app will not run without permission", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        permission();
                        button.setVisibility(View.VISIBLE);
                    }
                });
                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (databaseHelper.getGroupCount()>0){
            t1.setVisibility(View.GONE);
            t2.setVisibility(View.GONE);
            t3.setVisibility(View.GONE);
            t4.setVisibility(View.GONE);
            t5.setVisibility(View.GONE);
            t6.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            initComponents();
            header_tv.setText("Group List");
            relativeLayout.setBackgroundColor(Color.WHITE);
        }
    }
    public void refreshScreen(){
        customGrid.notifyDataSetChanged();
        gridView.invalidateViews();
        if (databaseHelper.getGroupCount()==0){
            t1.setVisibility(View.VISIBLE);
            t2.setVisibility(View.VISIBLE);
            t3.setVisibility(View.VISIBLE);
            t4.setVisibility(View.VISIBLE);
            t5.setVisibility(View.VISIBLE);
            t6.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            Drawable drawable = getResources().getDrawable(R.drawable.backg);
            relativeLayout.setBackground(drawable);
            initComponents();
            header_tv.setText("Group List");
        }
    }
    public void editGroup(int id,String name,String type,byte[] image,int c){
        groupAddDialog1 = new GroupAddDialog();
        groupAddDialog1.setData(String.valueOf(id),name,type,image,c);
        groupAddDialog1.show(getSupportFragmentManager(),"Group");
    }

}
