package com.lemosys.Call2Q.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.lemosys.Call2Q.GroupActivity;

public class ResetDialog extends DialogFragment {

    ResetDialogListener resetDialogListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            resetDialogListener = (GroupActivity) context;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are You Sure")
                .setPositiveButton("Re-Queue List", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        resetDialogListener.onSuccess();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it

        return builder.create();
    }

    public interface ResetDialogListener{
        public void onSuccess();
    }
}
