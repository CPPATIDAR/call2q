package com.lemosys.Call2Q.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.lemosys.Call2Q.Pojos.ContactModel;
import com.lemosys.Call2Q.Pojos.GroupModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "group.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_GROUP = "group_tb";
    private static final String KEY_ID = "id";
    private static final String GROUP_NAME = "group_name";
    private static final String GROUP_TYPE = "group_type";
    private static final String GROUP_Icon = "group_icon";
    private static final String GROUP_MCOUNT = "group_mcount";

    private static final String TABLE_CONTACT = "contact_tb";
    private static final String ID = "id";
    private static final String CONTACT_NAME = "name";
    private static final String CONTACT_NUMBER = "number";
    private static final String IsActive  = "flag";
    private static final String MGroup_ID = "group_id";
    private static final String IsCalled = "isCalled";
    private static final String IsDisconnected = "isDisconnected";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_GROUP_TABLE = "CREATE TABLE " + TABLE_GROUP + "(" + KEY_ID + " INTEGER PRIMARY KEY," + GROUP_NAME + " TEXT, " + GROUP_TYPE + " TEXT, "
                + GROUP_Icon + " BLOB," + GROUP_MCOUNT + " INTEGER " + ")";
        sqLiteDatabase.execSQL(CREATE_GROUP_TABLE);

        String CREATE_CONTACT_TABLE = "CREATE TABLE " + TABLE_CONTACT + "(" + ID + " INTEGER PRIMARY KEY, " + CONTACT_NAME + " TEXT, " + CONTACT_NUMBER + " TEXT, "
                + IsActive + " TEXT, " + MGroup_ID + " TEXT, " + IsCalled + " TEXT, " + IsDisconnected + " TEXT " + ")";
        sqLiteDatabase.execSQL(CREATE_CONTACT_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);
    }

    public int addGroup(String name,String type,byte[] icon,int count){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_NAME,name);
        contentValues.put(GROUP_TYPE,type);
        contentValues.put(GROUP_Icon,icon);
        contentValues.put(GROUP_MCOUNT,count);

        sqLiteDatabase.insert(TABLE_GROUP,null,contentValues);
        String query = "SELECT * FROM " + TABLE_GROUP;
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        cursor.moveToLast();
//        Log.e("query", "addGroup: "+cursor.getInt(0) + query );
        int id = cursor.getInt(0);
        sqLiteDatabase.close();
        return id;
    }

    public List<GroupModel> getGroup(){
        List<GroupModel> model = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectquery = "SELECT * FROM " + TABLE_GROUP ;
        Cursor cursor = sqLiteDatabase.rawQuery(selectquery,null);
        if (cursor.moveToFirst()){
            do {
                GroupModel groupModel = new GroupModel();
                groupModel.setId(cursor.getInt(0));
                groupModel.setName(cursor.getString(1));
                groupModel.setType(cursor.getString(2));
                groupModel.setIcon(cursor.getBlob(3));
                groupModel.setCount(cursor.getInt(4));
                model.add(groupModel);
            }while (cursor.moveToNext());
        }
        return model;
    }

    public int  getGroupCount(){
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectquery = "SELECT * FROM " + TABLE_GROUP ;
        Cursor cursor = sqLiteDatabase.rawQuery(selectquery,null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
    public List<GroupModel> getSingleGroup(String id){
        List<GroupModel> model = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String sql ="SELECT * FROM group_tb WHERE id = '"+ id+"'";
        Log.e("sql", sql );
        Cursor cursor = sqLiteDatabase.rawQuery(sql,null);
        Log.e("cursor count===", cursor.getCount()+"" );
        if (cursor.moveToFirst()){
            do {
                GroupModel groupModel = new GroupModel();
                groupModel.setId(cursor.getInt(0));
                groupModel.setName(cursor.getString(1));
                groupModel.setType(cursor.getString(2));
                groupModel.setIcon(cursor.getBlob(3));
                groupModel.setCount(cursor.getInt(4));
                model.add(groupModel);
            }while (cursor.moveToNext());
        }
        return model;
    }

    public void addContact(ContactModel contactModel){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",contactModel.getName());
        contentValues.put("number",contactModel.getNumber());
        contentValues.put("group_id",contactModel.getGroup_id());
        contentValues.put("flag","true");
        contentValues.put("isCalled","false");
        contentValues.put("isDisconnected","true");
        sqLiteDatabase.insert(TABLE_CONTACT,null,contentValues);
        sqLiteDatabase.close();
    }

    public List<ContactModel> getAllContact(int id){
        List<ContactModel> contactModelList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE group_id = '" + id + "'" + " AND isCalled = 'false' ";
        Log.e("query", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.moveToFirst()){
            do {
                ContactModel contactModel = new ContactModel();
                contactModel.setId(String.valueOf(cursor.getInt(0)));
                contactModel.setName(cursor.getString(1));
                contactModel.setNumber(cursor.getString(2));
                contactModel.setIsActive((cursor.getString(3)));
                contactModel.setGroup_id((cursor.getString(4)));
                contactModel.setCalled(Boolean.parseBoolean(cursor.getString(5)));
                contactModel.setIsDisconnected((cursor.getString(6)));
                contactModelList.add(contactModel);
            }while (cursor.moveToNext());
        }
        return contactModelList;
    }

    public void updateMcount(int number,String name){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_MCOUNT,number);
        sqLiteDatabase.update(TABLE_GROUP,contentValues,"group_name='" + name + "'",null);
    }

    public boolean checkContact(String number,int group_id){
        boolean flag= false;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT +" WHERE number = '" + number + "'" + " AND group_id = '" + group_id +"'";
        Log.e("query", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.getCount()>0){
            flag=true;
        }else {
            flag=false;
        }
        return flag;
    }

    public void updateIsActive(String value,int id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IsActive,value);
        sqLiteDatabase.update(TABLE_CONTACT,contentValues,"id='" + id + "'",null);
    }

    public void updateIsCalled(int id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IsCalled,"true");
        sqLiteDatabase.update(TABLE_CONTACT,contentValues,"id = '" + id + "'",null);
    }

    public void updateIsDisconnected(int id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IsDisconnected,"false");
        sqLiteDatabase.update(TABLE_CONTACT,contentValues,"id = '" + id + "'",null);
    }

    public List<ContactModel> getContactWithIsDisconnected(String id,boolean isDisconnected,boolean flag){
        List<ContactModel> contactModelList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE group_id = '" + id + "'" + " AND isDisconnected = '" + isDisconnected +"'" + " AND flag = '" + flag +"'";
        Log.e("bquery", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.moveToFirst()){
                ContactModel contactModel = new ContactModel();
                contactModel.setId(String.valueOf(cursor.getInt(0)));
                contactModel.setName(cursor.getString(1));
                contactModel.setNumber(cursor.getString(2));
                contactModel.setIsActive((cursor.getString(3)));
            contactModel.setGroup_id((cursor.getString(4)));
                contactModel.setCalled(Boolean.parseBoolean(cursor.getString(5)));
                contactModel.setIsDisconnected((cursor.getString(6)));
                contactModelList.add(contactModel);
        }
        return contactModelList;
    }

    public boolean getIsCalled(int id){
        boolean iscalled = false;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE id = '" + id +"'";
        Log.e("iscalled", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.moveToFirst()){
            iscalled = Boolean.parseBoolean(cursor.getString(5));
        }
        Log.e("iscalled", String.valueOf(iscalled));
        return iscalled;
    }

    public boolean getIsDisconnected(int id){
        boolean isDisconnected = false;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE id = '" + id +"'";
        Log.e("iscalled", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.moveToFirst()){
            isDisconnected = Boolean.parseBoolean(cursor.getString(6));
        }
        Log.e("iscalled", String.valueOf(isDisconnected));
        return isDisconnected;
    }

    public List<ContactModel> getContactWithIsConnected(String id, boolean iscalled){
        List<ContactModel> contactModelList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE group_id = '" + id + "'" + " AND isCalled = '" + iscalled +"'";
        Log.e("bquery", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.moveToFirst()){
            do {
                ContactModel contactModel = new ContactModel();
                contactModel.setId(String.valueOf(cursor.getInt(0)));
                contactModel.setName(cursor.getString(1));
                contactModel.setNumber(cursor.getString(2));
                contactModel.setIsActive((cursor.getString(3)));
                contactModel.setGroup_id((cursor.getString(4)));
                contactModel.setCalled(Boolean.parseBoolean(cursor.getString(5)));
                contactModel.setIsDisconnected((cursor.getString(6)));
                contactModelList.add(contactModel);
            }while (cursor.moveToNext());
        }
        return contactModelList;
    }
    public List<ContactModel> getDisconnectedFalse(String id,boolean isDisconnected){
        List<ContactModel> contactModelList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE group_id = '" + id + "'" + " AND isDisconnected = '" + isDisconnected +"'";
        Log.e("bquery", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.moveToFirst()){
            do {
                ContactModel contactModel = new ContactModel();
                contactModel.setId(String.valueOf(cursor.getInt(0)));
                contactModel.setName(cursor.getString(1));
                contactModel.setNumber(cursor.getString(2));
                contactModel.setIsActive((cursor.getString(3)));
                contactModel.setCalled(Boolean.parseBoolean(cursor.getString(5)));
                contactModel.setIsDisconnected((cursor.getString(6)));
                contactModelList.add(contactModel);
            }while (cursor.moveToNext());
        }
        return contactModelList;
    }

    public void deleteContact(String number) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_CONTACT,CONTACT_NUMBER + "='" + number + "'",null);

        db.close();
    }

    public void updateCallTable(){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IsDisconnected,"true");
        Log.e("call", "updateCallTable: " );
        sqLiteDatabase.update(TABLE_CONTACT,contentValues,"flag = 'true' AND isCalled = 'false' ",null);
    }

    public int getMCoun(int id){
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE group_id = '" + id + "'";
        Log.e("query", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        return cursor.getCount();
    }

    public void resetAll(String id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("flag","true");
        contentValues.put("isCalled","false");
        contentValues.put("isDisconnected","true");

        sqLiteDatabase.update(TABLE_CONTACT,contentValues,"group_id = '" + id +"'",null);
    }

    public void deleteGroup(String id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_GROUP,"id = '" + id + "'",null);
        sqLiteDatabase.delete(TABLE_CONTACT, "group_id = '" + id + "'",null);
    }

    public void editGroup(String id,String name,String Type,byte[] icon,int count){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ID,id);
        contentValues.put(GROUP_NAME,name);
        contentValues.put(GROUP_TYPE,Type);
        contentValues.put(GROUP_Icon,icon);
        contentValues.put(GROUP_MCOUNT,count);
        sqLiteDatabase.update(TABLE_GROUP,contentValues,"id = '" + id + "'",null);
    }

    public void addContactTalked(ContactModel contactModel){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",contactModel.getName());
        contentValues.put("number",contactModel.getNumber());
        contentValues.put("group_id",contactModel.getGroup_id());
        contentValues.put("flag","true");
        contentValues.put("isCalled","true");
        contentValues.put("isDisconnected","false");
        sqLiteDatabase.insert(TABLE_CONTACT,null,contentValues);
        sqLiteDatabase.close();
    }

    public int getAllDisconnect(int id){
        int count=0;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE group_id = '" + id + "'" + " AND isCalled = '" + true +"'" + " AND flag = '" + true +"'";
        Log.e("query", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.moveToFirst()){
            do {
              count++;
            }while (cursor.moveToNext());
        }
        return count;
    }

    public int getAllIsDisconnect(String id){
        int count=0;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CONTACT + " WHERE group_id = '" + id + "'" + " AND isDisconnected = '" + true +"'" + " AND flag = '" + true +"'";
        Log.e("bquery", query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if (cursor.moveToFirst()){
            do {
                count++;
            }while (cursor.moveToNext());
        }
        return count;
    }
}
