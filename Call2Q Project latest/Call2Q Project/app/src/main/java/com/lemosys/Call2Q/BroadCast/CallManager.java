package com.lemosys.Call2Q.BroadCast;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;


import com.lemosys.Call2Q.Database.DatabaseHelper;
import com.lemosys.Call2Q.GroupActivity;
import com.lemosys.Call2Q.Pojos.ContactModel;

import java.util.ArrayList;
import java.util.List;

public class CallManager extends BroadcastReceiver {

    TelephonyManager telManager;
    Context context;
    DatabaseHelper databaseHelper;
    List<ContactModel> list = new ArrayList<>();
    String id,number;
    boolean fla = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        databaseHelper = new DatabaseHelper(context);
        Log.e("intent", "onReceive: "+intent.getAction() );

        if (intent.getAction().equals("com.lemosys.Call2Q.BroadCast")) {
            id = intent.getStringExtra("id");
            number = intent.getStringExtra("no");
            Log.e("record", "onReceive: ---->>" + id);
        }

        telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    private final PhoneStateListener phoneListener = new PhoneStateListener() {

        @Override
        public void onCallStateChanged(int state, String phoneNumber) {
            super.onCallStateChanged(state, phoneNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING: {
                    break;
                }
                case TelephonyManager.CALL_STATE_OFFHOOK: {

//                    fla=true;
                    Log.e("TAG", "onCallStateChanged: "+id );
                    GroupActivity.groupActivity.updateData();
//                    if (id!=null){
//
//                        databaseHelper.updateIsDisconnected(Integer.parseInt(id));
//                    }
                    break;
                }
                case TelephonyManager.CALL_STATE_IDLE: {
                   if (fla==false){
//                       fla=true;
//                       startCall();

                   }
                   break;
                }
            }
        }
    };

    private void startCall() {
        if (id!=null){
            if (databaseHelper.getIsDisconnected(Integer.parseInt(id))==true){
                Intent intent_call = new Intent(Intent.ACTION_CALL);
                intent_call.setData(Uri.parse("tel:" + number));
                intent_call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                    }
                    context.startActivity(intent_call);
                }
        }
    }

    public interface connectListener{
        public void callConnect();
    }


}
