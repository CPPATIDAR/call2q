package com.lemosys.Call2Q;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.lemosys.Call2Q.Adapter.RecyclerAdapter;
import com.lemosys.Call2Q.Database.DatabaseHelper;
import com.lemosys.Call2Q.Pojos.ContactModel;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public class ContactList extends AppCompatActivity {

    private  String[] PROJECTION = new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
    List<ContactModel> contactModels = new ArrayList<>();
    List<ContactModel> sendtemp = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    DatabaseHelper databaseHelper;
    TextView reset;
    TextView header_tv,done;
    FloatingSearchView searchView;
    String group_id,group_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        getAllContacts();
        sortContact();
        intiView();
        listener();
    }

    public void getAllContacts(){
        ContentResolver cr = getApplicationContext().getContentResolver(); //Activity/Application android.content.Context
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,PROJECTION , null, null, null);
        if(cursor.moveToFirst())
        {
            contactModels = new ArrayList<>();
            do
            {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{ id }, null);
                while (pCur.moveToNext())
                {
                    String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String contactName = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    ContactModel pojo = new ContactModel(contactName,contactNumber);
                    contactModels.add(pojo);
                    break;
                }
                pCur.close();
            } while (cursor.moveToNext()) ;
        }

    }
    public void intiView(){
        searchView = findViewById(R.id.searchbox);
        recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(contactModels,ContactList.this);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.notifyDataSetChanged();
        databaseHelper = new DatabaseHelper(getApplicationContext());
        header_tv = findViewById(R.id.header_tv);
        reset = findViewById(R.id.reset);
        header_tv.setText("Contact List");
        Intent intent = getIntent();
        group_id = intent.getStringExtra("g_id");
        group_name = intent.getStringExtra("name");
        reset.setVisibility(View.GONE);

    }

    private void listener(){
        searchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
               filter(newQuery);
            }
        });
    }

    private void sortContact(){

        //Remove Duplicate
        HashSet<ContactModel> hashSet = new HashSet<ContactModel>();
        hashSet.addAll(contactModels);
        contactModels.clear();
        contactModels.addAll(hashSet);

        //Sort the
        Collections.sort(contactModels, new Comparator<ContactModel>() {
            @Override
            public int compare(ContactModel lhs, ContactModel rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
    }

    public void getSelectedItem(ContactModel contactModel,boolean flag){
        if (flag==false){
            Log.e("Selected",contactModel.getName()+contactModel.getNumber());
            sendtemp.add(contactModel);
        }else {
            Log.e("Removed",contactModel.getName()+contactModel.getNumber());
            sendtemp.remove(new ContactModel(contactModel.getName(),contactModel.getNumber()));
        }

    }

    public void setDataAndExit(){
        for (int i =0;i<sendtemp.size();i++){
            if (databaseHelper.checkContact(sendtemp.get(i).getNumber(), Integer.parseInt(group_id))==false){
                databaseHelper.addContact(new ContactModel(sendtemp.get(i).getName(),sendtemp.get(i).getNumber(),group_id));
            }

        }
        int size = databaseHelper.getAllContact(Integer.parseInt(group_id)).size();
        databaseHelper.updateMcount(size,group_name);
        finish();
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        List<ContactModel> listfound = new ArrayList<>();

        //looping through existing elements
        for (ContactModel s : contactModels) {
            //if the existing elements contains the search input
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                listfound.add(s);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        recyclerAdapter.filterList(listfound);
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


}
