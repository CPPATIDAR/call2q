package com.lemosys.Call2Q.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lemosys.Call2Q.AddGroupActivity;
import com.lemosys.Call2Q.GroupActivity;
import com.lemosys.Call2Q.ImageCompression;
import com.lemosys.Call2Q.R;
import com.valdesekamdem.library.mdtoast.MDToast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;



import static android.app.Activity.RESULT_OK;

public class GroupAddDialog extends DialogFragment {

    EditText name,type;
    Button submit,edit;
    ImageView img,change;
    byte[] image ;
    ProgressBar progressBar;
    ImageCompression imageCompression;
    String group_name,group_type,id,imageFilePath;
    int count;
    dialogListener mdialogListener;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mdialogListener = (AddGroupActivity) context;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public GroupAddDialog setData(String id, String name, String type, byte[] image,int count){
        this.id = id;
        this.group_name = name;
        this.group_type = type;
        this.image = image;
        this.count = count;
        return null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        imageCompression = new ImageCompression(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogview = inflater.inflate(R.layout.add_group,null);
        builder.setView(dialogview);

        progressBar = dialogview.findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
        name = dialogview.findViewById(R.id.et);
        type = dialogview.findViewById(R.id.et1);
        img = dialogview.findViewById(R.id.img);
        change = dialogview.findViewById(R.id.change);
        submit = dialogview.findViewById(R.id.btn_gp);
        edit = dialogview.findViewById(R.id.bt_gp_edit);
        if (TextUtils.isEmpty(group_name)){
                edit.setVisibility(View.GONE);
        }else {
            edit.setVisibility(View.VISIBLE);
            name.setText(group_name);
            type.setText(group_type);
            if (image!=null){
                Bitmap bitmap = getImage(image);
                img.setImageBitmap(bitmap);
            }
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String na =name.getText().toString();
                    String tp = type.getText().toString();
                    if (na.length()>0 && tp.length()>0  ){
                        mdialogListener.onEditClick(name.getText().toString(),type.getText().toString(),image,id,count);
//                        Intent intent = new Intent(getActivity(), GroupActivity.class);
//                        intent.putExtra("name",name.getText().toString());
//                        intent.putExtra("group_id",id);
//                        Log.e("key", "onClick: "+id );
//                        startActivity(intent);
                    }else {
                        MDToast.makeText(getActivity(),"Please Enter Name and Type",MDToast.LENGTH_SHORT,MDToast.TYPE_ERROR).show();
                    }
                }
            });

        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String na =name.getText().toString();
                String tp = type.getText().toString();
                if (na.length()>0 && tp.length()>0  ){

                   int id = mdialogListener.onSubmitClick(name.getText().toString(),type.getText().toString(),image);
                    Intent intent = new Intent(getActivity(), GroupActivity.class);
                    intent.putExtra("name",name.getText().toString());
                    intent.putExtra("group_id",id);
                    startActivity(intent);
                }else {
                    MDToast.makeText(getActivity(),"Please Enter Name and Type",MDToast.LENGTH_SHORT,MDToast.TYPE_ERROR).show();
                }

            }
        });

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(view.getRootView().getContext());
                alertbox.setMessage("Select an Option");
                alertbox.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int l) {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if(cameraIntent.resolveActivity(getActivity().getPackageManager()) != null){
                            //Create a file to store the image
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File
                            }
                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(getContext(),"com.lemosys.Call2Q",photoFile);
                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                        photoURI);
                                startActivityForResult(cameraIntent,
                                        0);
                            }
                        }
                    }
                });
                alertbox.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int l) {
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        if(galleryIntent.resolveActivity(getActivity().getPackageManager()) != null){
                            //Create a file to store the image
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                // Error occurred while creating the File
                            }
                            if (photoFile != null) {
                                Uri photoURI = FileProvider.getUriForFile(getContext(),"com.lemosys.Call2Q",photoFile);
                                galleryIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                        photoURI);
                                startActivityForResult(galleryIntent,1);
                            }
                        }

                    }
                });
                alertbox.show();
//                Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i,1);
            }
        });

        return builder.create();
    }

    public  interface dialogListener{
        int onSubmitClick(String name,String type, byte[] img);
        int onEditClick(String name,String type, byte[] img,String id,int count);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 0:
                if(resultCode == RESULT_OK ){

                    String image_path = imageCompression.compressImage(imageFilePath);
                    Bitmap bitmap = BitmapFactory.decodeFile(image_path);
                    bitmap = Bitmap.createBitmap(bitmap);
                    image = getBytes(bitmap);
                    Glide.with(this).load(imageFilePath).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                            return false;
                        }
                    }).into(img);
                   // img.setImageBitmap(bitmap);

                }
            case 1:
                if(resultCode == RESULT_OK && data!=null){
                    Uri uri = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getActivity().getContentResolver().query(uri,filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    String pic = imageCompression.compressImage(picturePath);

                    Bitmap bitmap1 = BitmapFactory.decodeFile(pic);
                    bitmap1 = Bitmap.createBitmap(bitmap1);
                    image = getBytes(bitmap1);

                    Glide.with(this).load(picturePath).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                            return false;
                        }
                    }).into(img);

                }
        }
    }

        // convert from bitmap to byte array
        public  byte[] getBytes(Bitmap bitmap) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
            image = stream.toByteArray();
            return stream.toByteArray();
        }

        public Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
        }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        imageFilePath = image.getAbsolutePath();
        return image;
    }

}
