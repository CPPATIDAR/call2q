package com.lemosys.Call2Q;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Handler;
import android.provider.CallLog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.telecom.Call;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;
import com.lemosys.Call2Q.Adapter.RecyclerAdapterTalked;
import com.lemosys.Call2Q.Adapter.RecyclerGroupAdapter;
import com.lemosys.Call2Q.Adapter.RecyclerSwipeFeature;
import com.lemosys.Call2Q.Adapter.RecyclerSwipeFeatureTalked;
import com.lemosys.Call2Q.BroadCast.CallManager;
import com.lemosys.Call2Q.Database.DatabaseHelper;

import com.lemosys.Call2Q.Dialog.AddContactDialog;
import com.lemosys.Call2Q.Dialog.ContactDialogOption;
import com.lemosys.Call2Q.Dialog.ResetDialog;
import com.lemosys.Call2Q.Pojos.CallInfo;
import com.lemosys.Call2Q.Pojos.ContactModel;
import com.lemosys.Call2Q.Pojos.GroupModel;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GroupActivity extends AppCompatActivity implements ContactDialogOption.ContactDialogListener,
        AddContactDialog.AddContactListener, RecyclerSwipeFeature.RecyclerSwipeListener,RecyclerSwipeFeatureTalked.SwipeListenertalked, ResetDialog.ResetDialogListener {

    DatabaseHelper databaseHelper;
    List<GroupModel> models;
    ImageView profile;
    MaterialLetterIcon materialLetterIcon;
    TextView reset;
    TextView group_name,group_count,header_tv,done,no_tv,start_call,stop_call,tv_call2q,tv_talked;
    Button addbutton;
    ContactDialogOption contactDialogOption;
    ResetDialog resetDialog;
    List<ContactModel> talked_lists = new ArrayList<>();
    AddContactDialog addContactDialog;
    RecyclerView recyclerView,recyclerView_talked;
    RecyclerGroupAdapter recyclerGroupAdapter;
    RecyclerAdapterTalked recyclerAdapterTalked;
    ProgressBar progressBar;
    boolean elsecheck = false;
    boolean onResumeCalled = false;

    BroadcastReceiver br;
    public static boolean  check = false;
    String name,id;
    String index;
    RelativeLayout relative_group;

    List<ContactModel> list = new ArrayList<>();

    public static GroupActivity groupActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        groupActivity = this;
        intializeView();
        setData();
        initListener();
        MobileAds.initialize(this, "ca-app-pub-5615829142679905~9312512301");
    }

    public void intializeView(){
        models = new ArrayList<>();
        profile = findViewById(R.id.profile_image);
        progressBar = findViewById(R.id.progress_group);
        group_name = findViewById(R.id.tv_group_name);
        group_count = findViewById(R.id.tv_group_count);
        materialLetterIcon = findViewById(R.id.material_icon);
        addbutton = findViewById(R.id.add_button);
        header_tv = findViewById(R.id.header_tv);
        reset = findViewById(R.id.reset);
        done = findViewById(R.id.done);
        no_tv = findViewById(R.id.no_tv);
        done.setVisibility(View.GONE);
        recyclerView = findViewById(R.id.recycler_group);
        start_call = findViewById(R.id.start_call);
        stop_call = findViewById(R.id.stop_call);
        relative_group = findViewById(R.id.relative_group);
        recyclerView_talked = findViewById(R.id.recycler_talked);
        tv_talked = findViewById(R.id.tv_talked);
        tv_call2q = findViewById(R.id.tv_call2q);


        progressBar.setVisibility(View.GONE);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerSwipeFeature(0,ItemTouchHelper.LEFT,this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback2 = new RecyclerSwipeFeatureTalked(0,ItemTouchHelper.LEFT,this);

        new ItemTouchHelper(itemTouchHelperCallback2).attachToRecyclerView(recyclerView_talked);
    }
    public Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }
    public void setData(){
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        int color = intent.getIntExtra("color",0);
        id = String.valueOf(intent.getIntExtra("group_id",0));
        databaseHelper = new DatabaseHelper(getApplicationContext());
        models = databaseHelper.getSingleGroup(id);
        materialLetterIcon.setLetter(name);
        materialLetterIcon.setShapeColor(color);
        header_tv.setText(name);
        if (models.size()>0){
            if (models.get(0).getIcon()==null){
                profile.setVisibility(View.INVISIBLE);
            }else {
                Bitmap bitmap = getImage(models.get(0).getIcon());
                materialLetterIcon.setVisibility(View.INVISIBLE);
                profile.setImageBitmap(bitmap);
            }
        }

        updateMCountAndTV();
    }
    public void initListener(){
        stop_call.setVisibility(View.GONE);
        addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactDialogOption = new ContactDialogOption();
                contactDialogOption.show(getSupportFragmentManager(),"show");
            }
        });
        start_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<ContactModel> list = new ArrayList<>();
                check = true;
                list = getRecord(true);
                if (list.size()>0){
                    stop_call.setVisibility(View.VISIBLE);
                    start_call.setVisibility(View.GONE);
                    index = list.get(0).getId();
                    Log.e("index", "Start : "+index );
                    callLoop(list.get(0).getId(),list.get(0).getNumber());
                    registerBroadcast(list.get(0).getId(),list.get(0).getNumber());
                    MDToast.makeText(getApplicationContext(),"Calling",MDToast.LENGTH_SHORT,MDToast.TYPE_SUCCESS).show();

                }
                else {
                    LocalBroadcastManager.getInstance(GroupActivity.this).unregisterReceiver(br);
                    check=false;
                }
            }
        });
        stop_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check= false;
                stop_call.setVisibility(View.GONE);
                start_call.setVisibility(View.VISIBLE);
                LocalBroadcastManager.getInstance(GroupActivity.this).unregisterReceiver(br);
                MDToast.makeText(getApplicationContext(),"Calling Stopped",MDToast.LENGTH_SHORT,MDToast.TYPE_SUCCESS).show();
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetDialog = new ResetDialog();
                resetDialog.show(getSupportFragmentManager(),"reset");
            }
        });
    }

    @Override
    public void addContact() {
        contactDialogOption.dismiss();
        addContactDialog = new AddContactDialog();
        addContactDialog.show(getSupportFragmentManager(),"contact");
    }

    @Override
    public void contactList() {
        contactDialogOption.dismiss();
        progressBar.getTop();
        progressBar.setVisibility(View.VISIBLE);
        Intent intent = new Intent(GroupActivity.this,ContactList.class);
        intent.putExtra("g_id",id);
        intent.putExtra("name",name);
        startActivity(intent);

    }

    @Override
    public void addContactData(String cname, String number) {
        addContactDialog.dismiss();
        ContactModel contactModel = new ContactModel(cname,number,id);
        if (databaseHelper.checkContact(number, Integer.parseInt(id))==false){
            databaseHelper.addContact(contactModel);
            setCall2QRecyclerView();
//            list = databaseHelper.getAllContact(Integer.parseInt(id));
//            recyclerGroupAdapter = new RecyclerGroupAdapter(list,getApplicationContext());
//            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//            recyclerView.setLayoutManager(layoutManager);
//            recyclerView.setAdapter(recyclerGroupAdapter);
//            recyclerGroupAdapter.notifyDataSetChanged();
            updateMCountAndTV();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateMCountAndTV();
        setCall2QRecyclerView();
        List<ContactModel> allist = new ArrayList<>();
        allist = getRecord(true);

        if(!onResumeCalled){
            onResumeCalled=true;
        if (check==true && allist.size()>0){
            Log.e("if", "getRecord: "+allist.get(0).getId()+allist.get(0).getNumber() );
            Log.e("if", "OnResume" );
            LocalBroadcastManager.getInstance(GroupActivity.this).unregisterReceiver(br);
            final List<ContactModel> finalList = allist;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateIsCalled();
                    setTalkedRecyclerView();
                    LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(br);
                    registerBroadcast(finalList.get(0).getId(), finalList.get(0).getNumber());
                    elsecheck=true;
                    index = finalList.get(0).getId();
                    Log.e("index", "Resume IFFFFF : "+index );
                    callLoop(finalList.get(0).getId(), finalList.get(0).getNumber());
                }
            },3000);
        } else {
                updateIsCalled();
                getIsDisconnected();
                databaseHelper.updateCallTable();
                if(elsecheck==true) {
                   // elsecheck=false;
                    List<ContactModel> allists = new ArrayList<>();
                    allists = getRecord(true);
                    if (check == true && allists.size() > 0) {

                        final List<ContactModel> finalAllists = allists;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                updateIsCalled();
                                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(br);
                                registerBroadcast(finalAllists.get(0).getId(), finalAllists.get(0).getNumber());
                                index = finalAllists.get(0).getId();
                                Log.e("index", "Resume ESLEE : "+index );
                                callLoop(finalAllists.get(0).getId(), finalAllists.get(0).getNumber());
                            }
                        }, 3000);
                    }else {
                        check= false;
                        stop_call.setVisibility(View.GONE);
                        start_call.setVisibility(View.VISIBLE);
                        LocalBroadcastManager.getInstance(GroupActivity.this).unregisterReceiver(br);
                        MDToast.makeText(getApplicationContext(),"Calling Stopped",MDToast.LENGTH_SHORT,MDToast.TYPE_SUCCESS).show();
                    }
                }
        }}
        setTalkedRecyclerView();

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(br);
        super.onPause();
        Log.e("record", "onPause" );
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(br);
        super.onDestroy();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof RecyclerGroupAdapter.MyViewHolder){
            final ContactModel deletedItem = list.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            recyclerGroupAdapter.removeItem(viewHolder.getAdapterPosition());
            updateMCountAndTV();
            Snackbar snackbar = Snackbar
                    .make(relative_group, deletedItem.getName() + " removed from list", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerGroupAdapter.restoreItem(deletedItem, deletedIndex);
                   updateMCountAndTV();
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();

        }
    }

    public void registerBroadcast(String id,String number){
        br = new CallManager();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
        registerReceiver(br, filter);


        Intent intent = new Intent("com.lemosys.Call2Q.BroadCast");
//        intent.putExtra("id",id);
//        intent.putExtra("no",number);
        sendBroadcast(intent);
    }

    public List<ContactModel> getRecord(boolean value){
        List<ContactModel> list = new ArrayList<>();
        list = databaseHelper.getContactWithIsDisconnected(id,value,true);
        Log.e("record", "getRecord: "+list.size() );
        return list;
    }

    public CallInfo getCallDuration(){
        CallInfo callInfo = new CallInfo();
        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE + " DESC");
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        sb.append("Call Details :");
        if (managedCursor.moveToFirst()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            int callDuration = Integer.parseInt(managedCursor.getString(duration));
            if (callType.equalsIgnoreCase("2")){
                callInfo = new CallInfo(phNumber,callDuration);
            }
        }
        Log.e("test", "getCallDuration: "+callInfo.getDuration() );
        return callInfo;
    }



    public void updateIsCalled(){
        List<ContactModel> lists = new ArrayList<>();
        lists = databaseHelper.getDisconnectedFalse(id,false);
        CallInfo callInfo =  getCallDuration();
        if (lists.size()>0 && callInfo.getNumber()!=null){
            Log.e("record", "updateIsCalled: "+lists.get(0).getNumber() + callInfo.getNumber() );

            for (int i=0;i<lists.size();i++){
                String no = lists.get(i).getNumber().replaceAll("[^0-9]","");
                String call_no = callInfo.getNumber().replaceAll("[^0-9]","");
                if (callInfo.getDuration()>0 && no.contains(call_no)){
                    Log.e("record", "CallInfo:---- "+callInfo.getNumber() +" <>"+ callInfo.getDuration() +" <> "+ lists.get(i).getId() );
                    databaseHelper.updateIsCalled(Integer.parseInt(lists.get(i).getId()));
                }
            }
        }
    }

    public void updateMCountAndTV(){
        progressBar.setVisibility(View.GONE);
        int size = databaseHelper.getMCoun(Integer.parseInt(id));
        databaseHelper.updateMcount(size,name);
        models = databaseHelper.getSingleGroup(id);
        if (size>=0){
            group_count.setText(String.valueOf(models.get(0).getCount()));
            reset.setVisibility(View.VISIBLE);
            no_tv.setVisibility(View.GONE);
            tv_talked.setVisibility(View.VISIBLE);
            tv_call2q.setVisibility(View.VISIBLE);
        }else {
            no_tv.setVisibility(View.VISIBLE);
            reset.setVisibility(View.GONE);
            tv_talked.setVisibility(View.GONE);
            tv_call2q.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(br);
    }
    public void getIsDisconnected(){
        List<ContactModel> list = new ArrayList<>();
        list = databaseHelper.getContactWithIsConnected(id,true);
        Log.e("tag", "getIsDisconnected: "+list.size() );
        for (int i=0;i<list.size();i++){
            Log.e("value", "getIsCalled:---- "+list.get(i).getNumber());
            Log.e("value", "getIsCalled: ----"+list.get(i).isCalled());
            Log.e("value", "getIsCalled: ----"+list.get(i).getIsDisconnected());
        }
    }


    @Override
    public void onSwipedListener(final RecyclerView.ViewHolder viewHolder, int direction, final int position) {

       // RecyclerView.ViewHolder holder = viewHolder;
        final ContactModel deletedItem = talked_lists.get(viewHolder.getAdapterPosition());
        final int deletedIndex = viewHolder.getAdapterPosition();
        recyclerAdapterTalked.removeItem(viewHolder.getAdapterPosition());
        updateMCountAndTV();
        Snackbar snackbar = Snackbar
                .make(relative_group, deletedItem.getName() + " removed from list", Snackbar.LENGTH_LONG);
        snackbar.setAction("UNDO", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerAdapterTalked.restoreItem(deletedItem, deletedIndex);
                updateMCountAndTV();
            }
        });
        snackbar.setActionTextColor(Color.YELLOW);
        snackbar.show();
    }

    @Override
    public void onSuccess() {
        databaseHelper.resetAll(id);
        setTalkedRecyclerView();
        setCall2QRecyclerView();
    }
    public void setCall2QRecyclerView(){
        list = databaseHelper.getAllContact(Integer.parseInt(id));
        if (Integer.parseInt(id)>0){
            recyclerGroupAdapter = new RecyclerGroupAdapter(list,getApplicationContext());
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(recyclerGroupAdapter);
            recyclerGroupAdapter.notifyDataSetChanged();
        }
    }
    public void setTalkedRecyclerView(){
        List<ContactModel> lists = new ArrayList<>();
        talked_lists = databaseHelper.getContactWithIsConnected(id,true);
        lists = databaseHelper.getContactWithIsConnected(id,true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView_talked.setLayoutManager(layoutManager);
            recyclerAdapterTalked = new RecyclerAdapterTalked(lists,getApplicationContext());
            recyclerView_talked.setAdapter(recyclerAdapterTalked);
            recyclerAdapterTalked.notifyDataSetChanged();
            setCall2QRecyclerView();
    }

    public void callLoop(String id,String number){
        if(check==true){
            if (databaseHelper.getIsDisconnected(Integer.parseInt(id))==true){

                Intent intent_call = new Intent(Intent.ACTION_CALL);
                intent_call.setData(Uri.parse("tel:" + number));
                intent_call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent_call);

            }
        }else {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(br);
        }
        }
        public  void updateData(){
        //Log.e("index", "updateData: "+index );
        databaseHelper.updateIsDisconnected(Integer.parseInt(index));
        onResumeCalled=false;
        }
}
