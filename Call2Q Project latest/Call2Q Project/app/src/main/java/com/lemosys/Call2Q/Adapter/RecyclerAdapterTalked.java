package com.lemosys.Call2Q.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.lemosys.Call2Q.Database.DatabaseHelper;
import com.lemosys.Call2Q.Pojos.ContactModel;
import com.lemosys.Call2Q.R;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class RecyclerAdapterTalked extends RecyclerView.Adapter<RecyclerAdapterTalked.MyViewHolder> {

    List<ContactModel> list = new ArrayList<>();
    Context context;
    DatabaseHelper databaseHelper;
    private int[] mMaterialColors;
    public CardView card_view;

    public RecyclerAdapterTalked(List<ContactModel> list, Context context) {
        this.list = list;
        this.context = context;
        databaseHelper = new DatabaseHelper(context);
        mMaterialColors = context.getResources().getIntArray(R.array.colors);
    }

    @NonNull
    @Override
    public RecyclerAdapterTalked.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_talked_layout,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final ContactModel contactModel = list.get(i);
        myViewHolder.materialLetterIcon.setLetter(contactModel.getName());
        myViewHolder.tv_name.setText(contactModel.getName());
        myViewHolder.aSwitch.setEnabled(false);
      //  card_view.setAlpha(0.5F);

        myViewHolder.rl_foreground_talked.setEnabled(false);

        myViewHolder.tv_number.setText(contactModel.getNumber());
        myViewHolder.aSwitch.setChecked(Boolean.parseBoolean(contactModel.getIsActive()));
        myViewHolder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b==false){
                    databaseHelper.updateIsActive("false", Integer.parseInt(contactModel.getId()));
                }else {
                    databaseHelper.updateIsActive("true", Integer.parseInt(contactModel.getId()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int adapterPosition) {
        if (list.size()>0){
            String num = list.get(adapterPosition).getNumber();
            databaseHelper.deleteContact(num);
            list.remove(adapterPosition);
            notifyItemRemoved(adapterPosition);
        }
    }

    public void restoreItem(ContactModel deletedItem, int deletedIndex) {

        list.add(deletedIndex,deletedItem);
        databaseHelper.addContactTalked(new ContactModel(deletedItem.getName(),deletedItem.getNumber(),deletedItem.getGroup_id()));
        notifyItemInserted(deletedIndex);
       // card_view.setAlpha(0.5F);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        MaterialLetterIcon materialLetterIcon;
        TextView tv_name,tv_number;
        RelativeLayout rl_foreground_talked;


        Switch aSwitch;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            card_view = itemView.findViewById(R.id.card_view);
            materialLetterIcon = itemView.findViewById(R.id.icon_talked);
            tv_name = itemView.findViewById(R.id.name_group_talked);
            tv_number = itemView.findViewById(R.id.number_group_talked);
            aSwitch = itemView.findViewById(R.id.swch_talked);
            rl_foreground_talked = itemView.findViewById(R.id.rl_foreground_talked);
            materialLetterIcon.setShapeColor(mMaterialColors[new Random().nextInt(mMaterialColors.length)]);
            materialLetterIcon.setLettersNumber(2);
            materialLetterIcon.setLetterSize(20);

        }
    }
}
