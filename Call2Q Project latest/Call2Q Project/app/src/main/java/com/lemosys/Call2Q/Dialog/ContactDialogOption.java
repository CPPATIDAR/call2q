package com.lemosys.Call2Q.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.lemosys.Call2Q.GroupActivity;
import com.lemosys.Call2Q.R;

public class ContactDialogOption extends DialogFragment {

    ContactDialogListener contactDialogListener;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogview = inflater.inflate(R.layout.contact_option_dialog,null);
        builder.setView(dialogview);
        Button add_con , con_list;

        add_con = dialogview.findViewById(R.id.btn_dialog_contact);
        con_list = dialogview.findViewById(R.id.btn_dialog_list);

        add_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactDialogListener.addContact();
            }
        });

        con_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactDialogListener.contactList();
            }
        });

        return builder.create();
    }

    public interface ContactDialogListener{
        void addContact();
        void contactList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            contactDialogListener = (GroupActivity) context;
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
