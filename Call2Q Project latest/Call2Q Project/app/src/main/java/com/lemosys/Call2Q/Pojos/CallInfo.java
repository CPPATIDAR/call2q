package com.lemosys.Call2Q.Pojos;

public class CallInfo {

    String number;
    int duration;

    public CallInfo(String number, int duration) {
        this.number = number;
        this.duration = duration;
    }

    public String getNumber() {
        return number;
    }

    public int getDuration() {
        return duration;
    }

    public CallInfo() {
    }
}
