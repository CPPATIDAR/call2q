package com.lemosys.Call2Q.Pojos;

public class GroupModel {

    int id;
    String name;
    String type;
    byte[] icon;
    int count;

    public GroupModel(String name, String type, byte[] icon, int count) {
        this.name = name;
        this.type = type;
        this.icon = icon;
        this.count = count;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public GroupModel() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public byte[] getIcon() {
        return icon;
    }

    public int getCount() {
        return count;
    }
}
