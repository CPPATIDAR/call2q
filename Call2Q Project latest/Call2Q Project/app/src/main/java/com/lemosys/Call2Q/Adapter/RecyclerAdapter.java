package com.lemosys.Call2Q.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lemosys.Call2Q.ContactList;
import com.lemosys.Call2Q.Pojos.ContactModel;
import com.lemosys.Call2Q.R;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    List<ContactModel> contactModelList = new ArrayList<>();
    Context context;
    private int[] mMaterialColors;
    TextView header_tv;
    List<ContactModel> temp = new ArrayList<>();

    public RecyclerAdapter(List<ContactModel> contactModelList, Context context) {
        this.contactModelList = contactModelList;
        this.context = context;
        mMaterialColors = context.getResources().getIntArray(R.array.colors);
        header_tv = ((ContactList)context).findViewById(R.id.done);
    }

    @NonNull
    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_adapter,viewGroup,false);
        return new  MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapter.MyViewHolder myViewHolder, int i) {
        final ContactModel contactModel = contactModelList.get(i);
        header_tv.setVisibility(View.GONE);
        myViewHolder.contact_icon.setLetter(contactModelList.get(i).getName());
        myViewHolder.contact_name.setText(contactModelList.get(i).getName());
        myViewHolder.number_contact.setText(contactModelList.get(i).getNumber());
        if (contactModel.isCalled()){
            myViewHolder.cardView.setBackgroundColor(R.drawable.background);
            header_tv.setVisibility(View.VISIBLE);
        }
        else {
            myViewHolder.cardView.setBackgroundColor(Color.WHITE);
        }
        if (temp.size()>0){
            header_tv.setVisibility(View.VISIBLE);
        }else if(temp.size()==0) {
            header_tv.setVisibility(View.GONE);
        }
        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactModel.setCalled(!contactModel.isCalled());
                if (contactModel.isCalled()) {
                    temp.add(contactModel);
                    ((ContactList)context).getSelectedItem(contactModel,false);
                }
                else {
                    ((ContactList)context).getSelectedItem(contactModel,true);

                    temp.remove(new ContactModel(contactModel.getName(),contactModel.getNumber()));
                }

                if (temp.size()>0){
                    header_tv.setVisibility(View.VISIBLE);
                }else if(temp.size()==0) {
                    header_tv.setVisibility(View.GONE);
                }
                myViewHolder.cardView.setBackgroundColor(contactModel.isCalled() ? R.drawable.background : Color.WHITE);
            }
        });
        header_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ContactList)context).setDataAndExit();
                ((ContactList)context).hideSoftKeyboard();
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactModelList.size();
    }

    public void filterList(List<ContactModel> filterdNames) {
        if (filterdNames!=null){
            this.contactModelList = filterdNames;
            notifyDataSetChanged();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        MaterialLetterIcon contact_icon;
        CardView cardView;
        TextView contact_name,number_contact;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            contact_icon = itemView.findViewById(R.id.icon_contact);
            contact_name = itemView.findViewById(R.id.name_contact);
            cardView = itemView.findViewById(R.id.cv_contact);

            header_tv.setVisibility(View.GONE);
            number_contact = itemView.findViewById(R.id.number_contact);
            contact_icon.setShapeColor(mMaterialColors[new Random().nextInt(mMaterialColors.length)]);
            contact_icon.setLettersNumber(2);
            contact_icon.setLetterSize(20);
        }
    }
}
