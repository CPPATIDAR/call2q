package com.lemosys.Call2Q.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.lemosys.Call2Q.GroupActivity;
import com.lemosys.Call2Q.R;
import com.hbb20.CountryCodePicker;
import com.valdesekamdem.library.mdtoast.MDToast;

public class AddContactDialog extends DialogFragment {
    AddContactListener addContactListener;
    String[] country = { "India", "USA", "China", "Japan", "Other"};
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogview = inflater.inflate(R.layout.add_contact_dialog,null);
        alertDialog.setView(dialogview);
        Button addContactBtn;
        Spinner spinner;
        final EditText name,number;

        final CountryCodePicker ccp =  dialogview.findViewById(R.id.ccp);
        name = dialogview.findViewById(R.id.contact_name);
        number = dialogview.findViewById(R.id.contact_number);
        addContactBtn = dialogview.findViewById(R.id.add_contact_btn);
        addContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().length()>0&& number.getText().toString().length()>0){
                    String no = ccp.getFullNumberWithPlus()+number.getText().toString();
                    addContactListener.addContactData(name.getText().toString(),no);
                }else {
                    MDToast.makeText(getActivity(),"Enter Valid number and name",MDToast.LENGTH_SHORT,MDToast.TYPE_ERROR).show();
                }

            }
        });



        return alertDialog.create();
    }

    public interface AddContactListener{
        void addContactData(String name,String number);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       try {
           addContactListener = (GroupActivity) context;
       }catch (Exception c){
           c.printStackTrace();
       }
    }
}
