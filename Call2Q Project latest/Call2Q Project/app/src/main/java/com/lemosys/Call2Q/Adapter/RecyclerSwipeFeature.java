package com.lemosys.Call2Q.Adapter;

import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

public class RecyclerSwipeFeature extends ItemTouchHelper.SimpleCallback {

    RecyclerSwipeListener recyclerSwipeListener;

    public RecyclerSwipeFeature(int dragDirs, int swipeDirs,RecyclerSwipeListener listener) {
        super(dragDirs, swipeDirs);
        this.recyclerSwipeListener = listener;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        recyclerSwipeListener.onSwiped(viewHolder,i,viewHolder.getAdapterPosition());

    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        View foreground = ((RecyclerGroupAdapter.MyViewHolder)viewHolder).foreground;
        getDefaultUIUtil().onDraw(c,recyclerView,foreground,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        View foreground = ((RecyclerGroupAdapter.MyViewHolder)viewHolder).foreground;
        getDefaultUIUtil().clearView(foreground);
    }

    @Override
    public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        View foreground = ((RecyclerGroupAdapter.MyViewHolder)viewHolder).foreground;
        getDefaultUIUtil().onDrawOver(c,recyclerView,foreground,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
        super.onSelectedChanged(viewHolder, actionState);
        if (viewHolder!=null){
          View foreground = ((RecyclerGroupAdapter.MyViewHolder)viewHolder).foreground;
          getDefaultUIUtil().onSelected(foreground);
        }
    }

    public interface RecyclerSwipeListener{
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
