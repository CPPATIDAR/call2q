package com.lemosys.Call2Q.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.lemosys.Call2Q.Database.DatabaseHelper;
import com.lemosys.Call2Q.Pojos.ContactModel;
import com.lemosys.Call2Q.R;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RecyclerGroupAdapter extends RecyclerView.Adapter<RecyclerGroupAdapter.MyViewHolder> {

    List<ContactModel> alContactModels= new ArrayList<>();
    Context context;
    DatabaseHelper databaseHelper;
    private int[] mMaterialColors;

    public RecyclerGroupAdapter(List<ContactModel> alContactModels, Context context) {
        this.alContactModels = alContactModels;
        this.context = context;
        mMaterialColors = context.getResources().getIntArray(R.array.colors);
        databaseHelper = new DatabaseHelper(context);
    }

    @NonNull
    @Override
    public RecyclerGroupAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_group,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerGroupAdapter.MyViewHolder myViewHolder, int i) {
        final ContactModel contactModel = alContactModels.get(i);
        myViewHolder.icon.setLetter(contactModel.getName());
        myViewHolder.name.setText(contactModel.getName());
        myViewHolder.number.setText(contactModel.getNumber());

        myViewHolder.aSwitch.setChecked(Boolean.parseBoolean(contactModel.getIsActive()));
        myViewHolder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b==false){
                    databaseHelper.updateIsActive("false", Integer.parseInt(contactModel.getId()));
                }else {
                    databaseHelper.updateIsActive("true", Integer.parseInt(contactModel.getId()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return alContactModels.size();
    }

    public void removeItem(int adapterPosition) {
        if (alContactModels.size()>0){
            String num = alContactModels.get(adapterPosition).getNumber();
            databaseHelper.deleteContact(num);
            alContactModels.remove(adapterPosition);
            notifyItemRemoved(adapterPosition);
        }

    }

    public void restoreItem(ContactModel deletedItem, int deletedIndex) {
        alContactModels.add(deletedIndex,deletedItem);
        databaseHelper.addContact(new ContactModel(deletedItem.getName(),deletedItem.getNumber(),deletedItem.getGroup_id()));
        notifyItemInserted(deletedIndex);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        MaterialLetterIcon icon;
        TextView name,number;
        RelativeLayout foreground,background;
        Switch aSwitch;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon_group_contact);
            name = itemView.findViewById(R.id.name_group);
            aSwitch = itemView.findViewById(R.id.swch);
            foreground = itemView.findViewById(R.id.foreground);
            background = itemView.findViewById(R.id.view_background);
            number = itemView.findViewById(R.id.number_group);
            icon.setShapeColor(mMaterialColors[new Random().nextInt(mMaterialColors.length)]);
            icon.setLettersNumber(2);
            icon.setLetterSize(20);
        }
    }
}
